import React from 'react';
const rrd = require('react-router-dom');

//global.fetch = require('node-fetch')
// Just render plain div with its children
rrd.BrowserRouter = ({children}) => <div>{children}</div>
module.exports = rrd;