import React from "react";
import { CircularProgress } from "@material-ui/core";

const Spinner = (props) => {
    return (
        <div className="spinnerContainer">
            <CircularProgress color="primary" size={props.size} thickness={props.thickness} />
        </div>
    );
};

export default Spinner;