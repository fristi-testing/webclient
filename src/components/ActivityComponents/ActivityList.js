import React from "react";
import ActivityItem from "./ActivityItem";
import ActivityForm from "./ActivityForm";

class ActivityList extends React.Component {
    /**
     *Vangt de state op die gegeven werd door het component ActivityContainer en vangt deze in een map op. Er wordt geitereert door de map
     * en elke activity in die map wordt voorgesteld door een ActivityItem
     */

    constructor(props) {
        super(props);
        this.handleEdit = this.handleEdit.bind(this);
        this.state = {
            itemEdited : null
        }
    }

    handleEdit(itemId) {
        console.log("editing..." + itemId);
        this.setState(this.state.itemEdited == itemId ? {itemEdited: null} : {itemEdited: itemId})
        this.props.onChange(true);
    }
    
    render() {
        return (
                <div className="activityList">
                    {this.props.activities && this.props.activities.map(
                        activity =>
                            this.state.itemEdited==activity.id?
                                <ActivityForm key={activity.id} activity={activity} onEdit={this.handleEdit} onChange={this.props.onChange} from={this.props.from} reload={this.props.reload}/>:
                                <ActivityItem key={activity.id} activity={activity} onEdit={this.handleEdit} onChange={this.props.onChange} from={this.props.from} reload={this.props.reload}/>
                    )}

                </div>
        )
    }
}

export default ActivityList