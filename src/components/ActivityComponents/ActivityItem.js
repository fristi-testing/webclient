import React from "react"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import DeleteActivityAlertDialog from "../Dialogs/DeleteActivityAlertDialog";


class ActivityItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open : false,
            link: ""
        };
    }

    /*
    addLink = () => {
        document.getElementById("link" + this.props.activity.id).href = (this.props.from === "dashboard" ?
            "/dashboard" : "")
            + "/activities/" + this.props.activity.id
    };

    removeLink = () => {
        document.getElementById("link" + this.props.activity.id).href = "";
    }
     */

    toggleCollapsed = () =>
    {
        this.props.onEdit(this.props.activity.id);
    };

    render(){
        const date = new Date(this.props.activity.startDate);
        return(
            <div className="activityItem">
                <div className="actitivyDescription col-sm-11" >
                    <div className="activityTop">
                        <div id={"flexTitle"}>
                            <Link
                                className="itemLink"
                                id={"link" + this.props.activity.id}
                                to={(this.props.from === "dashboard" ?
                                    "/dashboard" : "")
                                + "/activities/" + this.props.activity.id}>
                                <h2>{this.props.activity.name}</h2>
                            </Link>
                        </div>
                        {this.props.from === "dashboard" && <div id="buttonContainer">
                            <IconButton aria-label="edit" onClick={this.toggleCollapsed}>
                                <EditIcon fontSize="small" />
                            </IconButton>
                            <DeleteActivityAlertDialog onChange={this.props.onChange} activityId={this.props.activity.id} />
                        </div>}
                    </div>
                    <div>
                        <h3>{date.getDate()}-{date.getMonth()}-{date.getFullYear()} van {date.getHours()}:{String(date.getMinutes()).length < 2 && "0"}{String(date.getMinutes())} tot {date.getHours()}:{String(date.getMinutes()).length < 2 && "0"}{date.getMinutes()} @ {this.props.activity.location}</h3>
                        <p>{this.props.activity.description}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default ActivityItem
