import React from "react"

import '../../resource/css/ActivityPage.scss';

import FormLabel from 'react-bootstrap/FormLabel';
import FormControl from 'react-bootstrap/FormControl';
import '../../resource/css/ActivityPage.scss';

/* Dit is onze wrapper voor "FormControl", het react-bootstrap component dat we gebruiken voor de inputfields in forms */

class InputActivityItem extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <div >
            <div className="row">
                <div className="col-sm-4 hidden-xs">
                <FormLabel>
                    {this.props.name}
                </FormLabel>
                </div>
                <div className="col-xs-12 col-sm-8">
                    <FormControl type={this.props.type}  name={this.props.name} id={this.props.name} value={this.props.value} onChange={this.props.onChange}/>
                </div>
             </div>
            </div>
        );
    }
}

export default InputActivityItem;
