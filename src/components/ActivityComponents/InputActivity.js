import React from "react";
import ActivityForm from './ActivityForm'
import '../../resource/css/ActivityInputTable.scss'

class InputActivity extends React.Component {

    constructor(props){
        super(props);
        this.state={
            collapsed: true
        };

        this.toggleCollapsed = this.toggleCollapsed.bind(this);
    }

    toggleCollapsed(){
        this.setState({collapsed:!this.state.collapsed});
    }

    render() {
        return (
        <div className="activityCreator">
            {!this.state.collapsed &&
                <ActivityForm activity={this.props.activity} onEdit={this.toggleCollapsed}></ActivityForm>
                }
                {!this.state.collapsed && !this.props.activity &&
                    <input className="btn btn-primary" type="button" value="Sluiten" onClick={this.toggleCollapsed}/>
                }
                {this.state.collapsed && !this.props.activity &&
                    <input className="btn btn-primary" type="button" value="Nieuwe Activiteit" onClick={this.toggleCollapsed}/>
                }
         </div>
        )
    }
}

export default InputActivity