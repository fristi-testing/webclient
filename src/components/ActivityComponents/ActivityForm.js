import React from "react"
import DeleteActivityAlertDialog from "../Dialogs/DeleteActivityAlertDialog";
import InputActivityItem from "./InputActivityItem";
import Form from 'react-bootstrap/Form';
import SaveAlertDialog from "../Dialogs/SaveAlertDialog";

class ActivityItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = props.activity!=null ?
            {
                name: props.activity.name,
                startDate: props.activity.startDate,
                endDate: props.activity.endDate,
                location: props.activity.location,
                price: props.activity.price,
                ticketAmount: props.activity.ticketAmount,
                description: props.activity.description,
                image: props.activity.imageLink,
            } : {
                name: "",
                startDate: "",
                endDate: "",
                location: "",
                price: 0,
                ticketAmount: 0,
                description: "",
                image: "",
            };
        this.handleSubmit=this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleSubmit() {
        let data = {
            "id" : this.props.activity!=null?this.props.activity.id:null,
            "name" : this.state.name,
            "description" : this.state.description,
            "startDate" : this.state.startDate,
            "endDate" : this.state.endDate,
            "published" : true,
            "imageLink" : null,
            "amountOfLikes" : 0,
            "price" : this.state.price,
            "location" : this.state.location,
        };
        const url = this.props.activity!=null ?
            process.env.REACT_APP_API_URL + '/api/activities/'+this.props.activity.id :
            process.env.REACT_APP_API_URL + '/api/activities';
        fetch(url, {method: 'POST', headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
            .then(response => response.json())
            .then(data => {
                console.log('Success:', data);
            })
            .then(this.props.onEdit(this.props.key))
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    handleInputChange(event){
        const name = event.target.name;
        const value = event.target.value;
        this.setState({
            [name]: value
        });
    }

    render() {
        return (
            <Form>
                <InputActivityItem name="name" type="text" value={this.state.name} onChange={this.handleInputChange}>{}</InputActivityItem>
                <InputActivityItem name="startDate" type="datetime-local" value={this.state.startDate} onChange={this.handleInputChange}></InputActivityItem>
                <InputActivityItem name="endDate" type="datetime-local" value={this.state.endDate} onChange={this.handleInputChange}></InputActivityItem>
                <InputActivityItem name="location" type="text" value={this.state.location} onChange={this.handleInputChange}></InputActivityItem>
                <InputActivityItem name="price" type="text" value={this.state.price} onChange={this.handleInputChange}></InputActivityItem>
                <InputActivityItem name="ticketAmount" type="text"  onChange={this.onInputChange}></InputActivityItem>
                <InputActivityItem name="description" type="text" value={this.state.description} onChange={this.handleInputChange}></InputActivityItem>
                <InputActivityItem name="image" placeholder="Upload activiteit afbeelding" type="file" onChange={this.handleInputChange}></InputActivityItem>
                <div id="ButtonContainer">
                    {this.props.activity && <DeleteActivityAlertDialog toggleCollapsed={this.toggleCollapsed} handleDeleteActivity={this.handleDeleteActivity} /> }
                    <SaveAlertDialog toggleCollapsed={this.toggleCollapsed} onSave={this.handleSubmit}/>
                </div>
            </Form>
        )
    }

}

export default ActivityItem;