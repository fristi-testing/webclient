import React from "react";
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import EventIcon from '@material-ui/icons/Event';
import { withAuth0 } from "@auth0/auth0-react";
import ScheduleIcon from '@material-ui/icons/Schedule';
import ReservationForm from '../ReservationComponents/ReservationForm';
import Confirmation from '../ReservationComponents/Confirmation'
import '../../resource/css/ActivityDetail.scss';
import { Link } from "react-router-dom";

class ActivityDetailItem extends React.Component {
    state = {
        renderForm: false,
        isConfirmed:false,
        reservatie:{
            userId: "",
            amountOfExtraPersons:0,
            extraPersonen:[],
            paymentOffline: true,
            activity: null
        }
    };

    _onClick = (event) => {
        this.setState({renderForm: !this.state.renderForm});
    };

    onCompleteReservation = (reserv) => {
        console.log(this.state);
        const isConfirmed = true;
        this.setState({isConfirmed});
        console.log(this.state);
        const reservatie = {...reserv.data};
        this.setState({reservatie});
        console.log(this.state);
    };
    
    render() {
        const { isAuthenticated ,loginWithRedirect} = this.props.auth0;
        const startDate = new Date(this.props.activityInfo.startDate);
        const endDate = new Date(this.props.activityInfo.endDate);
        return(
            <div className="activityDetailItem">
                { this.props.activityInfo &&
                    <div>
                        <div className="pageTitle" id="activityHeader">
                            <Link className="backButton" to="../activities">
                                <Button >Terug naar activiteiten</Button>
                            </Link>
                            <h1>{this.props.activityInfo.name}</h1>
                            <img 
                                src={require("../../resource/images/" + this.props.activityInfo.imageLink + ".jpg").default}
                                alt={this.props.activityInfo.name + ": Afbeelding"}/>
                        </div>
                        <div className="activityLower">
                            <div className="activityDescription">
                                <h3>BESCHRIJVING</h3>
                                <p>{this.props.activityInfo.description}</p>
                                {!this.state.renderForm ? (isAuthenticated ?
                                    <Button variant="contained" onClick={this._onClick}>
                                               Schrijf je in
                                    </Button>
                                : <Button variant="contained" disabled>Schrijf je in</Button>) :
                                ""}
                                {isAuthenticated ? "" :
                                    <Link to="" onClick={() => loginWithRedirect()}>
                                        <span>Registreer of log je in om je in te kunnen schrijven.</span>
                                    </Link>}
                            </div>
                            <Paper className="activityDetails" elevation={1}>
                                <div>
                                <LocalOfferIcon color="primary" className="activityIcons" />
                                <p>€ {this.props.activityInfo.price}</p>
                                <br></br>
                                </div>
                                <div>
                                <EventIcon color="primary" className="activityIcons" />
                                <p>{startDate.getDate()}-{startDate.getMonth()}-{startDate.getFullYear()}</p>
                                <br></br>
                                </div>
                                <div>
                                <ScheduleIcon color="primary" className="activityIcons" />
                                <p>van {startDate.getHours()}:{String(startDate.getMinutes()).length < 2 && "0"}{String(startDate.getMinutes())} tot {endDate.getHours()}:{String(endDate.getMinutes()).length < 2 && "0"}{endDate.getMinutes()}</p>
                                <br></br>
                                </div>
                                <div>
                                <LocationOnIcon color="primary" className="activityIcons" />
                                <p>{this.props.activityInfo.location}</p>
                                </div>
                            </Paper>
                            <div id="reservationForm">
                            {
                                this.state.renderForm && this.state.isConfirmed && <Confirmation reservation={this.state.reservatie}/> || this.state.renderForm && <ReservationForm onConfirmed={this.onCompleteReservation} activity={this.props.activityInfo}/>
                            }
                        </div>
                        </div>
                    </div>
                }
            </div>
        );
    }
}

export default withAuth0(ActivityDetailItem);