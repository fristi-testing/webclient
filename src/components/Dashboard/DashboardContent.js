import React from "react";
import '../../resource/css/DashboardContent.css';
import {
    Route
  } from "react-router-dom";
import ActivityPage from './ActivityPage';
import UserPage from './UserPage';
import ActivityDetail from "../Pages/ActivityDetail";

const DashboardContent = (props) => {

    return(
        <div className="dashboardContent">
            <div className="pageTitle">
                <h1>{props.currentPage}</h1>
            </div> 
            <Route exact path="/dashboard/home"
            render={() => <p>Placeholder Text</p>}/>
            <Route exact path="/dashboard/activities"
            render={() => <ActivityPage />}/>
            <Route exact path="/dashboard/users"
            render={() => <UserPage {...props } name="Gebruikers"/>}/>
            <Route exact path="/dashboard/activities/:id" component={ActivityDetail}/>
        </div>

    );
};

export default DashboardContent;