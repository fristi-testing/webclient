import React, { useEffect, useState, useRef } from "react";
import '../../resource/css/Dashboard/UserPage.css';
import UserList from "./UserList";
import { Fetcher } from '../../utils/Fetcher.js';
import Searcher from './Searcher';
import Spinner from '../Spinner';
import RoleFilters from "./RoleFilters";

const UserPage = (props) => {
    const isComponentMounted = useRef(true);
    const { isLoading, error, data: users } = Fetcher("/api/users", isComponentMounted);
    const { isLoading: isLoadingRoles, error: errorRoles, data: allRoles, controller } = Fetcher("/api/users/roles", isComponentMounted);
    const [searchQuery, setSearchQuery] = useState("");
    const [nameFilter, setNameFilter] = useState({
      predicate: (user) => true,
      isFiltered: false
    });
    const [roleFilter, setRoleFilter] = useState([]);

  useEffect(() => {
    return () =>
      controller.abort();
  }, []);


  const handleSearchQueryChange = (e) => {
    setSearchQuery(e.target.value);
      let regEx = new RegExp(`.*` + e.target.value + `.*`, "ig");
      setNameFilter({
        predicate: (user) => user.name.match(regEx),
        isFiltered: true
      });
  }

  const handleRoleFilterChange = (e) => {
    let filter = [...roleFilter];
    if(e.target.checked) {
      filter.push(e.target.name);
    } else {
      filter = filter.filter((role) => role !== e.target.name);
    }
    setRoleFilter(filter);
  }

  if(isLoading || isLoadingRoles) {
      return(
        <Spinner size={80} thickness={4} />
      );
    } else if(error || errorRoles) {
        return(
          <div>
            <p>[Error] {error.status + " : " + error.statusText} (betere error message nog te implementeren)</p>
          </div>
        )
      } else {
          sessionStorage.setItem("allRoles", JSON.stringify(allRoles));
          const filteredUsers = users.filter(nameFilter.predicate).map((user) => user.user_id);
          return(
          <div>
            <Searcher onChange={handleSearchQueryChange} value={searchQuery}/>
            <RoleFilters onChange={handleRoleFilterChange} filteredRoles={roleFilter} />
            <UserList users={users} filteredUsers={filteredUsers} filteredRoles={roleFilter}/>
          </div>
        );
      }
}

export default UserPage;