import React from "react";

const ActivityFilterList = (props) => {

    return (
        <ul id="activityFilterList">
            {props.filterOptions.map((option) => {
                return <li key={"option_" + option} onClick={props.onClick}>{option}</li>
            })}
        </ul>
    );
}

export default ActivityFilterList;