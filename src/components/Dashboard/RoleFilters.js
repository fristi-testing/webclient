import React from "react";
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const RoleFilters = (props) => {

    const roles = JSON.parse(sessionStorage.getItem("allRoles"));

    return(
        <FormControl component="fieldset" className="roleFiltersContainer">
            <FormGroup>
                {roles.length > 0 && roles.map((role) =>
                    <FormControlLabel
                        key={"fcl_" + role.id}
                        style={{ // Zorgt ervoor dat enkel de checkbox klikbaar is en een dubbelklik geen selectie maakt.
                            pointerEvents: "none",
                            userSelect: "none", /* standard syntax */
                            WebkitUserSelect: "none", /* webkit (safari, chrome) browsers */
                            MozUserSelect: "none", /* mozilla browsers */
                            khtmlUserSelect: "none", /* webkit (konqueror) browsers */
                            msUserSelect: "none", /* IE10+ */ 
                        }} 
                        control= {
                            <Checkbox onChange={props.onChange} 
                                      name={role.id}
                                      style={{ pointerEvents: "auto" }}
                                      color="primary" />
                        }
                        label={role.name}
                    />
                )}

            </FormGroup>
        </FormControl>
    );
}

export default RoleFilters;