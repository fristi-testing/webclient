import React, { useEffect, useState} from "react";
import '../../resource/css/Dashboard/ActivityPage.css';
import ConditionalActivityList from './ConditionalActivityList';
import ActivityFilterList from './ActivityFilterList';
import InputActivity from "../ActivityComponents/InputActivity";

const ActivityPage = (props) => {
  const baseUrl = "/api/activities";
  const [url, setUrl] = useState(baseUrl);
  const [activitiesLoading, setActivitiesLoading] = useState(true);

  const filterUrls = {
    "Alles" : baseUrl,
    "Ongepubliceerd" : baseUrl + "/nonpublished",
    "Openstaand" : baseUrl + "/upcoming",
    "Afgelopen" : baseUrl + "/past"
  };

  useEffect(() => {
    setActivitiesLoading(true);
  }, [url]);

  const handleFilterListClick = (e) => {
    setUrl(filterUrls[e.target.innerText]);
  };

  const handleLoadingDone = () => {
    setActivitiesLoading(false);
  };
    return(
        <div className="dashBoardActivityContainer">
            {activitiesLoading ? "" :
              <ActivityFilterList 
                filterOptions={Object.keys(filterUrls)} 
                onClick={handleFilterListClick}/>
            }
            <InputActivity  key={0} height={200} width="80%"/>
            <ConditionalActivityList 
              key={url} 
              url={url} 
              onLoadingDone={handleLoadingDone} />
        </div>

    );
}

export default ActivityPage;