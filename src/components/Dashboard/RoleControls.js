import React from "react";
import Chip from '@material-ui/core/Chip';
import DoneIcon from '@material-ui/icons/Done';
import {Grow} from '@material-ui/core';

const RoleControls = (props) => {

    const allRoles = JSON.parse(sessionStorage.getItem("allRoles"));

    const toggleChip = (e) => {
        let classes;
        console.log(e.target.tagName === "SPAN");
        if(e.target.tagName === "SPAN"){
            classes = e.target.parentNode.classList;
        } else classes = e.target.classList;

        classes.toggle("inactive");
        classes.toggle("active_role");
    };

    if(props.editMode === false) {
        return ( 
            <div className="roleControlsContainer">
                {props.roles.length > 0 && props.roles.map(
                    role =>
                        <Chip size="small"
                            label={role.name} 
                            key={role.id} 
                            className="roleChip" />
                )}
                    <a 
                        style={props.roles.length > 0 ? {} : { marginLeft: 0 }}
                        onClick={props.onClick}>Rollen wijzigen</a>
            </div>
        );
    } else{
        console.log(props.userId);
    return (
        <Grow in={true}>
            <div className="roleControlsContainer" id={props.userId} >
                {allRoles && allRoles.map(
                    role =>
                    <Grow in={true} timeout={1000} key={"grow_edit_" + role.id}>
                        <Chip size="small"
                            label={role.name} 
                            key={role.id}
                            id={role.id}
                            onClick={toggleChip}
                            className={props.roles.some(r => r.name == role.name) 
                                    ? "roleChip active_role" : "roleChip inactive"}
                            style={ {cursor: "pointer"}} />
                    </Grow>
                )}
                <Grow in={true} timeout={2000}>
                    <a onClick={props.onClick}>
                        <div className="roleConfirmButtonContainer" id={"confirmButton_" + props.userId}>
                            <DoneIcon style={{ top: "-3", position: "relative" }}/>
                            <span>Opslaan</span>
                        </div>
                    </a>        
                </Grow>
            </div>
        </Grow>
    );
    }
};

export default RoleControls;