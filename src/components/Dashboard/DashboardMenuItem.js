import React from "react";
import '../../resource/css/DashboardMenuItem.css';
import { Link } from "react-router-dom";

const DashboardMenuItem = (props) => {
    
    const onClick = (e) => {
        props.onClick(props.name);
        if(!e.currentTarget.classList.contains('active')) {
            // Als het geklikte item niet het huidig actieve is, worden alle andere items op non-actief gezet
            // TODO: Omslachtig geschreven -> betere methode schrijven. Also, doorgeven van active page via parent?
            let siblings = getSiblings(e.currentTarget);
            for (let sibling of siblings) {
                if(sibling.classList.contains('active')) {
                    sibling.className = "dashBoardMenuItem";
                    sibling.parentNode.className = "";
                }
            }
            e.currentTarget.className += " active";
            e.currentTarget.parentNode.className += "active";
        }
    }

    const getSiblings = (target) => {
        let siblings = [];
        let sibling = target.parentNode.parentNode.firstElementChild;

        while(sibling) {
            if (sibling.nodeType === 1) {
                siblings.push(sibling.firstElementChild);
                sibling = sibling.nextElementSibling;
            }
        }
        return siblings;
        
    }

    return(
        <Link to={"/dashboard/" + props.link} className={(props.isActive ? " active" : "")}>
            <div className={"dashboardMenuItem" + (props.isActive ? " active" : "")} onClick={onClick}>
                <h2>{props.name}</h2>
            </div>
        </Link>
        
    );
}

export default DashboardMenuItem;