import React from "react";
import UserItem from "./UserItem";

const UserList = (props) => {

    return (
            <div className="userList">
                {props.filteredUsers.length === 0 ?
                    <p>Geen gebruikers gevonden.</p>
                    : ""}
                {props.users.length > 0 && props.users.map(
                    user =>
                        <UserItem 
                            key={user.user_id} 
                            user={user} 
                            filteredUsers={props.filteredUsers.length > 0 ? props.filteredUsers : []}
                            roleFilter={props.filteredRoles && props.filteredRoles.length > 0 ? props.filteredRoles : []}/>
                )}
            </div>
    );
}

export default UserList;