import React, { useEffect, useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import '../../resource/css/DashboardPage.css';
import DashboardMenuItem from "./DashboardMenuItem";
import DashboardContent from "./DashboardContent";
import { Roles } from "../../utils/RoleEnum";

const DashboardPage = (props) => {
  // TODO: Was even probleempje met <Link>, maar huidige pagina kan beter via Router.
  // En heel het page(name)-gedoe mag nog herschreven worden als er later tijd is, nogal een messy gedoe.
  const pageNames = {
    "home" : "Home",
    "activities" : "Activiteiten",
    "users" : "Gebruikers",
    "sponsors" : "Sponsoring",
    "registrations" : "Inschrijvingen"
  }
  const [currentPage, setCurrentPage] = useState();
  const { user, loginWithRedirect } = useAuth0();
  let roles = user ? user["http://oc/claims/roles"] : [];


  const handleMenuClick = (page) => {
    setCurrentPage(page);
  };

  useEffect(() => {
    setCurrentPage(pageNames[props.match.params.page]);
  }, [pageNames[props.match.params.page]]);
  
  // Niet-bemachtigde leden worden doorgestuurd naar de loginpagina.
  if(roles.includes(Roles.OC) || roles.includes(Roles.ADMIN)) { // TODO: Vervangen met RBAC access checks?
    return (
      <div>
        <div className="dashboardMenu">
          {Object.keys(pageNames).map(
            page => { 
            return <DashboardMenuItem
                      key={page} 
                      name={pageNames[page]} 
                      link={page} 
                      onClick={handleMenuClick}
                      isActive={pageNames[page] === currentPage ? true : false}/>
            }
          )}

        </div>
          <DashboardContent currentPage={currentPage}/>
      </div>

    );
  } else {
    loginWithRedirect()
  }
};

export default DashboardPage;