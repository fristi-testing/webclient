import React, {useRef, useEffect, useState} from "react";
import { Fetcher } from "../../utils/Fetcher";
import ActivityList from "../ActivityComponents/ActivityList";
import Spinner from '../Spinner';

const ConditionalActivityList = (props) => {
    const isComponentMounted = useRef(true);
    const [activitiesChanged,setActivitiesChanged] = useState(false);
    const { isLoading, error, data: activities, controller } = Fetcher(props.url, isComponentMounted,activitiesChanged);

    useEffect( () => {
        if(activitiesChanged) {
            setActivitiesChanged(false)
        }
    }, [activitiesChanged]);

    useEffect(() => {
        return () => controller.abort();
      }, []);

      useEffect(() => {
        if(!isLoading) {
            props.onLoadingDone();
        }
      }, [isLoading]);

    if(isLoading) {
        return <Spinner size={80} thickness={4} />
    } else if(error) {
        return (
            <div>
              <p>[Error] {error.status + " : " + error.statusText} (betere error message nog te implementeren)</p>
            </div>
        )
    } else {
        return <ActivityList activities={activities} from="dashboard" onChange={setActivitiesChanged} />
    }
};

export default ConditionalActivityList;