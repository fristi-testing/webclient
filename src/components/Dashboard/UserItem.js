import React, { useEffect, useState, useRef } from "react"
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { Fetcher } from '../../utils/Fetcher.js';
import { CircularProgress, Grow } from "@material-ui/core";
import RoleControls from "./RoleControls.js";
import { useAuth0 } from "@auth0/auth0-react";

const UserItem = (props) => {

  const isComponentMounted = useRef(true);

  const [rolesChanged, setRolesChanged] = useState(false);
  const [editingRoles, setEditingRoles] = useState(false);
  
  const { isLoading, error, data: userRoles, controller } = Fetcher("/api/users/" + props.user.user_id + "/roles", isComponentMounted, rolesChanged);

  const { getAccessTokenSilently } = useAuth0();

  // Reset de change-trigger.
  useEffect(() => {
    if(rolesChanged) setRolesChanged(false);
  }, [rolesChanged]);

  useEffect(() => {
    return () => controller.abort();
  }, [])

  // POST-request moet pas gestuurd worden bij toggleRoleChange, maar daar kan de Fetcher niet geplaatst worden
  const postUpdatedRoles = async (updatedRoles) => {
    const token = await getAccessTokenSilently();
    let response = await fetch(process.env.REACT_APP_API_URL + "/api/users/" + props.user.user_id + "/roles", {
      headers: {
          Authorization: `Bearer ${token}`,
          Accept: 'application/json',
          'Content-Type': 'application/json' 
      },
      method: 'POST',
      body: JSON.stringify(updatedRoles)
      });
    console.log("Called API to add roles; response:" + response);

    return response;
  }

  const deleteUpdatedRoles = async (updatedRoles) => {
    const token = await getAccessTokenSilently();
    let response = await fetch(process.env.REACT_APP_API_URL + "/api/users/" + props.user.user_id + "/roles", {
      headers: {
          Authorization: `Bearer ${token}`,
          Accept: 'application/json',
          'Content-Type': 'application/json' 
      },
      method: 'DELETE',
      body: JSON.stringify(updatedRoles)
      });
    console.log("Called API to delete roles; response:" + response);

    return response;
  }

  function wait(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  const toggleRoleChange = async (e) => {
    if(editingRoles) {

      let addedRoles = { roles: [] };
      let removedRoles = { roles: [] };
      let userRoleChips = document.getElementById(props.user.user_id).children;

      console.log(userRoleChips);
      Array.from(userRoleChips).forEach((el) => {
        if(el.classList.contains("active_role") && userRoles.filter(ur => (ur.id === el.id)).length === 0) {
          addedRoles.roles.push(el.id);
        } else if(el.classList.contains("inactive") && userRoles.some(ur => ur.id === el.id)){
          removedRoles.roles.push(el.id);
        } 
      });
      console.log("Added roles: " + addedRoles.roles);
      let postResponse = {
        ok: true
      };
      let deleteResponse = {
        ok: true
      };
      if(addedRoles.roles.length > 0) postResponse = await postUpdatedRoles(addedRoles);

      console.log("Removed roles: " + removedRoles.roles);
      if(removedRoles.roles.length > 0) deleteResponse = await deleteUpdatedRoles(removedRoles);
      setRolesChanged(true);

      if(postResponse.ok && deleteResponse.ok) {
        //Event target hangt af van waar je precies klikt. Fiksbaar? Anders met QuerySelector.
        const confirmButton = document.getElementById("confirmButton_" + props.user.user_id);
        confirmButton.style.backgroundColor = 'limegreen';
        confirmButton.style.color = 'white';
        confirmButton.style.opacity = 0;
        confirmButton.lastChild.textContent = 'Opgeslagen';
      }

      wait(1500).then(() => { 
        setEditingRoles(false);
      });
    } else return setEditingRoles(true);
  }

  const shouldBeVisible = () => {
    let visible = true;

    if(props.filteredUsers) {
        visible = props.filteredUsers.includes(props.user.user_id);
    }
    if(visible && props.roleFilter.length > 0) {
        visible = props.roleFilter.every((role) => userRoles.some((ur) => ur.id === role));
    }

    return visible;
  }

  if(isLoading) {
    return (
      <div className="spinnerContainer">
        <CircularProgress color="primary" size="60" thickness={2} />
      </div>
    )
  } else if(error) {
      return (          
        <div>
          <p>[Error] {error} (betere error message nog te implementeren)</p>
        </div>
      )
  } else if(!shouldBeVisible()){
      return null;
  } else return(
      <Grow in={true} timeout={1000}>
      <Card className="userItemRoot" key={props.user.id}>
        <CardMedia
          className="userItemAvatar"
          image={props.user.picture}
          title="Avatar"
        />
        <div className="userItemDetails">
          <CardContent className="userItemContent">
            <h1>
              {props.user.name}
            </h1>
            <h2>
            {props.user.email}
            </h2>
            <RoleControls 
              editMode={editingRoles} 
              roles={userRoles} 
              onClick={toggleRoleChange}
              userId={props.user.user_id} />
          </CardContent>
        </div>
      </Card>
      </Grow>
  );
}

export default UserItem;