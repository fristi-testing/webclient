import React from "react";
import SearchIcon from '@material-ui/icons/Search';

const Searcher = (props) => {
    return(
        <div className="searchContainer">
            <SearchIcon />
            <input
                type="text"
                value={props.searchQuery}
                id="searchInput"
                placeholder="Zoek op naam..."
                onChange={props.onChange}
                autoComplete="off"
                label="Search"
            />
        </div>

    );
}

export default Searcher;