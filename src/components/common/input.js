import React from 'react';
import {Form,Row,Col} from 'react-bootstrap';

class  Input extends React.Component{
    render (){
        if(this.props.type === "checkbox"){
            return (<Form.Group as={Row} controlId={this.props.name}>
            <Col sm="10">
              <Form.Check type="checkbox" label={this.props.label}  value={this.props.value}/>
            </Col>
          </Form.Group>);
            }
            else if(this.props.type === "text"){
                return(
                <Form.Group as={Row} controlId={this.props.name}>
                                <Form.Label column sm="2">
              {this.props.label}
            </Form.Label>
                <Col sm="10">
                  <Form.Control autoFocus name={this.props.name} plaintext  value={this.props.value} onChange={this.props.onChange}/>
                </Col>
              </Form.Group>);
            }
    }
}

export default Input;