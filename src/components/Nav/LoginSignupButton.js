import React from "react";
import {useAuth0} from "@auth0/auth0-react";
import Button from "@material-ui/core/Button";

const LoginSignupButtons = () => {
    const {loginWithRedirect} = useAuth0();

    return <>
        <Button id="highlightButton" variant="contained"
                onClick={() => loginWithRedirect()}>LOG IN</Button>
        <>  </>

        <Button id="registerButton"
                onClick={() =>
                    loginWithRedirect({
                        mode: "signUp",
                    })
                }
        >Nog geen account?</Button>
    </>

};


export default LoginSignupButtons;