import React from "react";
import {
    Link
  } from "react-router-dom";
import '../../resource/css/NavBar.scss'
import Button from '@material-ui/core/Button';
import LoginControl from "./LoginControl";
import { useAuth0 } from '@auth0/auth0-react';
import { Roles } from '../../utils/RoleEnum.js';
import AuthenticationButton from "./AuthenticationButton";

const NavBar = () => {

    const { user } = useAuth0();
    let roles = user ? user["http://oc/claims/roles"] : [];

    // Check if path is error page - remove navbar
    if (window.location.pathname === "/404")
        return null

    return (
        <div className="navBar">
            <div className='navLogo'>
                <img 
                    src={require("../../resource/images/f_logo2.png").default}
                    alt="Logo" />
            </div>
            <div className='navLinks'>
                <ul>
                    <li>
                        <Link to="/">
                            <Button>
                                Over ons
                            </Button>
                        </Link>
                    </li>
                    <li>
                        <Link to="/activities">
                            <Button>
                                Activiteiten
                            </Button>
                        </Link>
                    </li>
                    {(roles.includes(Roles.OC) || roles.includes(Roles.FIN) || roles.includes(Roles.ADMIN)) 
                    && 
                    <li>
                        <Link to="/dashboard/home">
                            <Button>
                                Dashboard
                            </Button>
                        </Link>
                    </li>
                    }

                    <li>
                        {/*<LoginControl/>*/}
                        <AuthenticationButton/>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default NavBar;