import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import HighlightedButton from "./HighlightedButton";

const LogoutButton = () => {
    const { logout } = useAuth0();

    return (
        <HighlightedButton onClick={() => logout({ returnTo: window.location.origin })} text="Log Uit" />
    );
};

export default LogoutButton;