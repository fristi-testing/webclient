import React from "react";
import Button from '@material-ui/core/Button';

const HighlightedButton = (props) => {
return  <Button 
            color={props.color ? "" : "primary"}
            style={props.color ? { backgroundColor: props.color } : {}} 
            id="highlightButton" 
            variant="contained" 
            onClick={props.onClick}>
            {props.text}
        </Button>;
};

export default HighlightedButton;