import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {Link} from "react-router-dom";
import {useAuth0} from "@auth0/auth0-react";
import '../../resource/css/NavBar.scss'
import ImageAvatars from "./AvatarButton";
import '../../resource/css/ProfilePage.css'

export default function ProfileButton() {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const { logout } = useAuth0();

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className="profileButtonContainer">
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                {/*<AuthenticationButton />*/}
                <ImageAvatars />
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
                getContentAnchorEl={null}
                anchorOrigin={{vertical: 'bottom', horizontal: 'center'}}
                transformOrigin={{vertical: 'top', horizontal: 'center'}}

            >
                <Link to="/profile"  className="dropdownMenuItems dropdownProfile"><MenuItem onClick={handleClose}>Profiel</MenuItem></Link>
                <Link to="/"  className="dropdownMenuItems dropdownProfile"><MenuItem onClick={handleClose}>Mijn inschrijvingen</MenuItem></Link>
                <MenuItem className="dropdownProfile" onClick={() => logout({ returnTo: window.location.origin })}>Logout</MenuItem>
            </Menu>
        </div>

    );
}