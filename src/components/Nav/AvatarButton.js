import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import { useAuth0 } from "@auth0/auth0-react";



const ImageAvatars = () => {

    const { user } = useAuth0();
    const { picture } = user;

    return (
        <div>
            <Avatar alt="picture" src={picture} />
        </div>
    );
}
export default ImageAvatars;