import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import HighlightedButton from "./HighlightedButton";

const LoginButton = () => {
    const { loginWithRedirect } = useAuth0();

    return <HighlightedButton onClick={() => loginWithRedirect()} text="Log In" />;
};

export default LoginButton;