import { withAuth0 } from "@auth0/auth0-react";
import React from "react"
import LoginButton from "./LoginButton";
import LogoutButton from "./LogoutButton";
import Button from '@material-ui/core/Button';

class LoginControl extends React.Component {

    render() {
        const { isAuthenticated, loginWithRedirect } = this.props.auth0;
      let button;
      let registerLink;
      if (isAuthenticated) {
        button = <LogoutButton/>;
      } else {
        button = <LoginButton/>;
        registerLink = <Button id="registerButton"
          onClick={() =>
              loginWithRedirect({
                  mode: "signUp",
              })
          }
          >Nog geen account?</Button>;
      }
  
      return (
        <div>
          {button}
          {registerLink}
        </div>
      );
    }
  }
  export default withAuth0(LoginControl);