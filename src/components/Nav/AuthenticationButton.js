import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import LoginSignupButtons from "./LoginSignupButton";
import ProfileButton from "./ProfileButton";

const AuthenticationButton = () => {
    const { isAuthenticated } = useAuth0();

    return isAuthenticated ? <ProfileButton /> : <LoginSignupButtons /> ;
}

export default AuthenticationButton;