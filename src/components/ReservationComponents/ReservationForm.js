import React from "react";
import GuestListContainer from "./GuestListContainer";
import {Form,Button} from 'react-bootstrap';
import { withAuth0 } from "@auth0/auth0-react";
import Input from '../common/input';
import axios from 'axios';


class ReservationForm extends React.Component {
    state = {
        reservatie:{
            userId: "",
            amountOfExtraPersons:0,
            extraPersonen:[],
            paymentOffline: true,
            activity: null
        },
        amount:0
    }

    componentDidMount(){
        if(this.props !== undefined){
            const { activity } = this.props;
            const reservatie = {...this.state.reservatie};
            reservatie.activity = activity;
            this.setState({reservatie});
        }
    }


    handleAdd = (guest) => {
        if(this.state.reservatie.amountOfExtraPersons > this.state.reservatie.extraPersonen.length){
            const extraPersonen = [...this.state.reservatie.extraPersonen , guest]
            const reservatie = {...this.state.reservatie};
    
    
            if(extraPersonen.length == null)
            {
                reservatie.amountOfExtraPersons = [...this.state.reservatie.amountOfExtraPersons]
            }
            else
            {
                reservatie.extraPersonen = extraPersonen;
            }

            this.setState({ reservatie});
        }



    };

    removeGuest = index => {
        const extraPersonen = [...this.state.reservatie.extraPersonen];
        extraPersonen.splice(index,1);
        const reservatie = {...this.state.reservatie};
        reservatie.extraPersonen = extraPersonen;
        this.setState({ reservatie});
    };

    handleOnclick = async (event) => {
        event.preventDefault();
        if(this.state.reservatie.amountOfExtraPersons != this.state.reservatie.extraPersonen.length){
            const guest = [...this.state.reservatie.extraPersonen]
            for(let i = 0 ; i< (this.state.reservatie.amountOfExtraPersons - this.state.reservatie.extraPersonen.length) ; i++){
                guest.push("naamloos")
            }
            const reservatie = {...this.state.reservatie};
            reservatie.extraPersonen = guest;
            this.setState({reservatie});
        }
        const accessToken = await this.props.auth0.getAccessTokenSilently();
        const act = await axios.post(process.env.REACT_APP_API_URL + '/api/reservations',this.state.reservatie,
        {
            headers: {
              'Authorization': `Basic ${accessToken}` 
            }
          });
          this.props.onConfirmed(act);
    };

    handleChange = ({currentTarget: input}) => {
        const reservatie = {...this.state.reservatie};
    
        reservatie[input.name] = input.value;
        this.setState({reservatie});
      }
    render()
    {
        return(
            <Form data-testid="formControlCheckBox" onSubmit={this.handleOnclick}>
                <Input data-testid="checkBoxPayment" name="paymentOffline" value={this.state.reservatie.paymentOffline} label="Offline betalen" type="checkbox" onChange={this.handleChange}/>
                <Input data-testid="inputAmount" name="amountOfExtraPersons" value={this.state.reservatie.amountOfExtraPersons} type="text" label="Aantal guests" onChange={this.handleChange}/>
                <GuestListContainer removeGuest={this.removeGuest}onAdd={this.handleAdd} extraPersonen={this.state.reservatie.extraPersonen} amount={this.state.reservatie.amountOfExtraPersons}/>
                <Button data-testid="saveBtn" type="submit">Save Reservatie</Button>
            </Form>)
    }

}

export default withAuth0(ReservationForm);