import React, {useState} from 'react'
import GuestList from "./GuestList";

function GuestListContainer(props){
const [newGuest,setNewGuest] = useState('');



function addGuest(e){
    e.preventDefault();
    const extraPers = [...props.extraPersonen, newGuest];

    props.onAdd(newGuest);
  setNewGuest("");
}



/*useEffect(() => {
    setGuests(mockData)
    }
   async function fetchData() {
        const result = await fetch('http://localhost:3000/reservaties').then((response) =>

        response.json()
        )
        setGuests(result);
    }
    fetchData();
}, []);*/

    function handleChange(event) {
        setNewGuest(event.target.value);
      }

return (
    <div className="App">
        <div>
            <input data-testid="inputGuest" type="text" value={newGuest} onChange={handleChange}/>
            <button id="addGuest" onClick={addGuest}>Add Guest</button>
        </div>
        <h4>Your Guest List</h4>
        {<GuestList
            guests={props.extraPersonen}
            removeGuest={props.removeGuest}/>}
    </div>
);
}


export default GuestListContainer;