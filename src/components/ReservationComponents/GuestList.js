import React from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
    const GuestList = ({ guests,removeGuest }) =>
        <div>
            {
                guests
                .map((guest,i) =>
                        ( <div key={i}>{guest} <IconButton aria-label="delete" onClick={(e) => (e.preventDefault(),removeGuest(i))}><DeleteIcon fontSize="small"/></IconButton></div> )
                    )
            }
        </div>;

    export default GuestList;




 