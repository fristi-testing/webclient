import React from "react";

    const Confirmation = ({reservation}) =>
        <div>
            <h3>Your Reservation is Confirmed</h3>
            <h5>Resume :</h5>
            <p>Offline betaald : {reservation.paymentOffline ? "Ja" : "Nee"}</p>
            <p>Aantal deelnemers : {reservation.amountOfExtraPersons}</p>
            <p>Lijst Deelnemers : </p>
            {
                
                reservation.extraPersonen.map((person,i) => (
                    <p key={i}>{person}</p>
                ))
            }
        </div>

    export default Confirmation;
