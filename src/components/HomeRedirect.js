import React from "react";
import { Redirect } from "react-router-dom";
import { Roles } from '../utils/RoleEnum';
import { useAuth0 } from '@auth0/auth0-react';
import { CircularProgress } from '@material-ui/core';

const HomeRedirect = () => {

    const { user } = useAuth0();
    let roles = user ? user["http://oc/claims/roles"] : [];

    if(user) {
        return(
            <Redirect to={roles.includes(Roles.OC) || roles.includes(Roles.ADMIN) ? 
                "/dashboard/home" : "/" }
            />
        );
    } else {
        return (
        <div className="spinnerContainer" id="redirectSpinner">
            <CircularProgress color="primary" size="100" thickness={5} />
        </div>
        );
    }
};

export default HomeRedirect;