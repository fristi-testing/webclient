import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import SaveIcon from '@material-ui/icons/Save';
import IconButton from "@material-ui/core/IconButton";
import ActivityItem from "../ActivityComponents/ActivityItem";
import DeleteIcon from "@material-ui/icons/Delete";

class SaveAlertDialog extends React.Component {
    state = {
        open: false
    };
    constructor(props) {
        super(props);
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClickDelete = () => {
        this.setState({ open: true });
    };

    handleConfirmed = () => {
        console.log("Me too!");
        this.props.onSave();
        this.setState({ open: false });
    };
    handleBack = () => {
        this.setState({ open: false });

    };

    handleAgree = () => {
        console.log("I agree!");
        this.handleConfirmed();
    };
    handleDisagree = () => {
        console.log("I do not agree.");
        this.handleBack();
    };
    render() {
        return (
            <div>
                {/* Button to trigger the opening of the dialog */}

                <IconButton aria-label="save" onClick={this.toggleCollapsed}>
                    <SaveIcon fontSize="medium" onClick={this.handleClickOpen}/>
                </IconButton>

                {/* Dialog that is displayed if the state open is true */}
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">
                        {"Wijziging annuleren"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Ben je zeker dat je de wijzigingen wil opslaan?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleDisagree} color="primary">
                            Neen
                        </Button>
                        <Button onClick={this.handleAgree} color="primary" autoFocus>
                            Ja
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default SaveAlertDialog;
