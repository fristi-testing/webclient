import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";

class DeleteActivityAlertDialog extends React.Component {
    state = {open: false
    };
    constructor(props) {
        super(props);
    }

    handleClickDeleteIcon = () => {
        this.setState({ open: true });
    };

    handleDeleteActivity = async () => {
        try {
            fetch(process.env.REACT_APP_API_URL + "/api/activities/" + this.props.activityId, {method: 'DELETE'}).then(this.props.onChange(true));
        } catch (error) {
            console.log("Error bij het deleten van de activiteit");
            console.log(error.message);
        }
    };

    handleDelete = () => {
        this.setState({ open: false });
        this.handleDeleteActivity();
    };

    handleDontDelete = () => {
        this.setState({ open: false });
    };

    handleAgree = () => {
        console.log("I agree!");
        this.handleDelete();
    };

    handleDisagree = () => {
        console.log("I do not agree.");
        this.handleDontDelete();
    };

    render() {
        return (
            <div>
                {/* Button to trigger the opening of the dialog */}
                <IconButton aria-label="delete" onClick={this.toggleCollapsed} >
                    <DeleteIcon fontSize="medium" onClick={this.handleClickDeleteIcon} />
                </IconButton>
                {/* Dialog that is displayed if the state open is true */}
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">
                        {"Activiteit verwijderen"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Ben je zeker dat je de activiteit wil verwijderen?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleDisagree} color="primary">
                            Neen
                        </Button>
                        <Button onClick={this.handleAgree} color="primary" autoFocus>
                            Ja
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default DeleteActivityAlertDialog;
