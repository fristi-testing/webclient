import React, {useRef, useEffect, useState} from "react";
import ActivityList from "../ActivityComponents/ActivityList"
import '../../resource/css/ActivityPage.scss';
import { Fetcher } from '../../utils/Fetcher.js';
import Spinner from "../Spinner";

const ActivityPage = () => {
  const isComponentMounted = useRef(true);
  const [activitiesChanged,setActivitiesChanged] = useState(false);
  const { isLoading, error, data: activities } = Fetcher("/api/activities/upcoming", isComponentMounted, activitiesChanged);

    useEffect( () => {
        if(activitiesChanged) {
            setActivitiesChanged(false)
        }
    }, [activitiesChanged]);

  if(isLoading) {
    return (
      <div>
            <Spinner size={80} thickness={4} />
      </div>
    )
  } else if(error) {
      return (
        <div>
            <p>[Error] {error.status + " : " + error.statusText} (betere error message nog te implementeren)</p>
        </div>
      )
  } else return (
    <div>
      <div className="pageTitle">
          <h1>Activiteiten</h1>
      </div>
      <ActivityList onChange={setActivitiesChanged} activities={activities} from="activities" />
    </div>
  )
};

export default ActivityPage;