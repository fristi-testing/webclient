import React from "react";
import Button from "@material-ui/core/Button";

const ProfileBoxContent = (props) => {

    return (
        <div className='profileBoxContent'>
            <form onSubmit={props.onSubmit}>
                <h5>Naam</h5>
                <label>Voornaam</label>
                <div>
                    <input
                        id="voornaamInput"
                        name="voornaam"
                        type="text"
                        placeholder={props.profileInfo.given_name ? props.profileInfo.given_name : " "}
                        className="form-control"
                    />
                    <p/>
                </div>
                <label>Achternaam</label>
                <div>
                    <input
                        id="achternaamInput"
                        name="achternaam"
                        type="text"
                        placeholder={props.profileInfo.family_name ? props.profileInfo.family_name : " "}
                        className="form-control"
                    />
                    <p/>
                </div>
                <div>
                    <h5>Gebruikersnaam</h5>
                    {/*<p>Opgelet, als u uw gebruikersnaam wijzigt, verandert dat hoe u zich aanmeldt.</p>*/}
                    {/*<label>Gebruikersnaam</label>*/}
                    <div>
                        <p>{props.profileInfo.name? props.profileInfo.name : " "}</p>
                    </div>
                    <p/>
                </div>

                <div>
                    <h5>Email</h5>
                    <p>{props.profileInfo.email? props.profileInfo.email :  ""}</p>
                    <p/>
                </div>

                <h5>Taal</h5>
                <p>In welke taal wil u berichten van ons ontvangen?</p>
                <div className="form-group">
                    <div className="field">
                        <select
                            id="taal"
                            name="taal"
                            type="text"
                            className="form-control"
                            defaultValue={props.profileInfo.user_metadata ? props.profileInfo.user_metadata.preferred_language : "NL"}
                        >
                            <option value="NL">NL</option>
                            <option value="FR">FR</option>
                            <option value="EN">EN</option>
                        </select>
                    </div>
                </div>
                <Button id="loginButton" variant="contained" type="submit">Update</Button>
            </form>
        </div>
    )
}

export default ProfileBoxContent;