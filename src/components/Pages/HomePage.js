import React from "react"
import '../../resource/css/HomePage.scss'
import { Fade } from "@material-ui/core";
class HomePage extends React.Component{

    render(){
     return (
         <Fade in={true} timeout={1200}>
            <div className="homeContainer">
                <div className="homeText">
                    <h1>Oudercomité</h1> 
                    <h1>GBS De Kleine Wijzer</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <a href="./activities">
                        Bekijk onze activiteiten
                    </a>
                </div>
                <div className="homeImg">
                    <img alt="Spelende kinderen" src={require("../../resource/images/home_img.png").default}/>
                </div>
            </div>
        </Fade>
           );
    }
}

export default HomePage;
