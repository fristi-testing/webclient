import React from "react";

class ErrorPage extends React.Component{
    render(){
        return (
            <div>
                <h1 style={{textAlign:"center"}}> This is an ERROR  </h1>
                    <p style={{textAlign:"center"}}>
                        <a href="/">Go to Home</a>
                    </p>
            </div>
        )
    }
}
export default ErrorPage;