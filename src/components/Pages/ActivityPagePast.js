import React, { useEffect, useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import ActivityList from "../ActivityComponents/ActivityList"
import '../../resource/css/ActivityPage.scss';
import InputActivity from "../ActivityComponents/InputActivity";

const ActivityPage = () => {
  const [message, setMessage] = useState("");
  const [error, setError] = useState("");


  const { getAccessTokenSilently, isAuthenticated } = useAuth0();
  const fetchPastActivities = async () => {
    try {
      setError(null);
       const token = await getAccessTokenSilently();
      console.log(token);
      const response = await fetch("/api/activities/past", {
        headers: {

            Authorization: `Bearer ${token}`
                    }
        });

      if(!response.ok) {
        throw new Error(response.status + ": " + response.statusText);
      }
      
      const responseData = await response.json();
      setMessage(responseData);

    } catch (error) {
      console.log("Error bij fetchen van data:");
      console.log(error.message);
      setError(error.message);
    }
  };

  useEffect(() => {
    fetchPastActivities();
  }, []);

  if(isAuthenticated && error == null) {
  return (
    <div>
                    <div className="pageTitle">
                        <h1>Afgelopen Activiteiten</h1>
                    </div>
                    <ActivityList activities={message} />
                    <InputActivity height={200} width="80%" />

                </div>
  );
  } else return (
    <div>
                    <div className="pageTitle">
                        <h1>Pagina niet beschikbaar voor huidige gebruiker.</h1>
                    </div>
                    

                </div>
  )
};

export default ActivityPage;