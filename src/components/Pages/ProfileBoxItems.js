import React, {useState} from "react";
import {useAuth0} from "@auth0/auth0-react";
import ConfirmDialog from "../Dialogs/ConfirmDialog";

const ProfileBoxItems = () => {

    const { logout, user, getAccessTokenSilently } = useAuth0();
    const [confirmOpen, setConfirmOpen] = useState(false);

    const deleteUser = async () => {
        console.log("in deleteUser: username: " + user.name + " userid: " + user.sub + " user_id: " + user.user_id)
        const token = await getAccessTokenSilently();
        let response = await fetch("/api/users/" + user.sub, {
            headers: {
                Authorization: `Bearer ${token}`
            },
            method: 'DELETE'
        });
        console.log("Called API to delete user; response:" + response);
        logout();

        return response;
    }

    return(
        <div className="profileBoxItems" >
            <p></p>
            <p></p>
            <h5>Account</h5>

                <p>Ik wil mijn account <a href={"#"} onClick={() => setConfirmOpen(true)}>verwijderen</a>

                </p>
                <ConfirmDialog
                    title="Account verwijderen?"
                    open={confirmOpen}
                    setOpen={setConfirmOpen}
                    onConfirm={() => deleteUser()}
                >
                    Zeker dat u uw account wil verwijderen?
                </ConfirmDialog>

        </div>
    )
}

export default ProfileBoxItems;
