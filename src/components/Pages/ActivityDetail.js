import React, {useEffect, useRef, useState} from "react"
import ActivityDetailItem from "../ActivityComponents/ActivityDetailItem"
import {Fetcher} from "../../utils/Fetcher";
import Spinner from "../Spinner";

const ActivityDetail = (props) => {

    const [id] = useState(props.match.params.id);
    const isComponentMounted = useRef(true); // Om memoryleaks te voorkomen - Is uw component nog gemount
    const { isLoading, error, data: activity, controller } = Fetcher("/api/activities/" + id, isComponentMounted);

    useEffect(() => {
        return () =>
            controller.abort();
    }, []);

    if(isLoading) {
        return (
            <div>
                <Spinner size={80} thickness={4} />
            </div>
        )
    } else if(error) {
        return (
            <div>
                <p>[Error] {error.status + " : " + error.statusText} (betere error message nog te implementeren)</p>
            </div>
        )
    } else return (

        <div>
            <ActivityDetailItem activityInfo={activity} />
        </div>
    );

}

export default ActivityDetail