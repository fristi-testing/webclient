import React, {useEffect, useState} from "react";
import {useAuth0} from "@auth0/auth0-react";
import {Fetcher} from "../../utils/Fetcher2";
import '../../resource/css/DashboardPage.css';
import ProfileBoxContent from "./ProfileBoxContent";
import ProfileBoxItems from "./ProfileBoxItems";
import '../../resource/css/ProfilePage.css';
import {useHistory} from 'react-router-dom';


const ProfilePage = () => {

    const {isLoading, error, data: message} = Fetcher("/api/users/profile", "GET")
    const {getAccessTokenSilently, isAuthenticated, loginWithRedirect, logout, isLoading: isUserLoading} = useAuth0();
    const [mess, setMess] = useState({});
    const [err, setErr] = useState("");
    const history = useHistory();
    const user = useAuth0();

    const sendUserupdate = async (e) => {
        try {
            const voornaam = document.getElementById("voornaamInput");
            const achternaam = document.getElementById("achternaamInput");
            const taal = document.getElementById("taal");

            setErr(null);
            const token = await getAccessTokenSilently();

            let updatedUser = {
                //gedeelte na || toegevoegd om "bad request" op te lossen als alleen taal wordt aangepast, zonder given_name, given_name mag niet leeg zijn --> maar misschien bestaat er een betere oplossing
                given_name: voornaam.value || voornaam.getAttribute("placeholder") || "",
                family_name: achternaam.value || achternaam.getAttribute("placeholder") || "",
                //als er geen taal gekozen wordt, moet de huidige taal weergegeven worden
                user_metadata: {preferred_language: taal.value || taal[0].innerText}
            };
            await fetch(process.env.REACT_APP_API_URL + "/api/users/profile", {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                },
                body: JSON.stringify(updatedUser)
            })
                .then(response => {
                    return response.json()

                })
                .then(message => {
                    setMess(message);
                    console.log('Success: ', message);
                });
        } catch (err) {
            console.log("Error bij fetchen van data:");
            console.log(err.message);
            setErr(err.message);
        }
    };

    useEffect(() => {
    }, [message]);
//TODO: spinnner
if(isUserLoading) {
    return <p>aan het laden</p>
}
    else if(!isAuthenticated){
        //anders even foutmelding

        loginWithRedirect();
    }
    else if (isLoading) {
        return (
            <div>
                <p>Profiel wordt ingeladen ...</p>
            </div>
        )
    }


    else if (error) {
        return (
            <div>
                <p>[Error] {error.status + " : " + error.statusText}</p>
            </div>
        )
    }

    else {
        return (
            <div>
                <div className="pageTitle">
                    <h1>Profiel</h1>
                </div>
                <div className="colDisplay">
                    <ProfileBoxContent profileInfo={message} onSubmit={sendUserupdate}/>
                    <ProfileBoxItems/>
                </div>
            </div>
        )
    }
};

export default ProfilePage;