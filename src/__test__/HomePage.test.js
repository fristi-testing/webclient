import React from "react";
import Enzyme, {shallow} from 'enzyme';
import toJson from "enzyme-to-json";
import HomePage from "../components/Pages/HomePage";
import logo from "../resource/images/home_img.png";
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

let wrapper;

beforeAll(() => { wrapper = shallow(<HomePage/>); });

describe('Testing HomePage', () => {

        // SNAPSHOT TEST
        it('snapshot of HomePage', () => {
            expect(toJson(wrapper)).toMatchSnapshot();
        });

        // UNIT TEST
        it('renders the component',() => {
            expect(wrapper).not.toBe(null);
        });

        it('should have a div container',() =>{
            expect(wrapper.find('div.homeContainer')).toHaveLength(1);
        });

        it('includes a link to activities', () => {
            expect(wrapper.find('a').props().href).toBe('./activities');
            expect(wrapper.find('a').text()).toEqual('Bekijk onze activiteiten');
        });

        it('Check if img src is empty', () => {
            const img = wrapper.find('div.homeImg');
            expect(img.find('img').prop('src')).not.toBeNull();
        });

        it('should render the correct logo', () => {
            const img = wrapper.find('div.homeImg');
            expect(img.find('img').prop('src')).toEqual(logo);
        });

});