import React from 'react';
import {configure, shallow} from 'enzyme'
import NavBar from "../components/Nav/NavBar";
import logo from '../resource/images/f_logo2.png'
import toJson from "enzyme-to-json";
import Link from 'react-router'
import Button from "@material-ui/core/Button";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

let wrapper;

beforeAll( () => { wrapper = shallow(<NavBar />); })


describe('NavBarComponent', () => {

     // SNAPSHOT TEST
    it('snapshot of NavBar', () => {
            expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('Testing NavBar Components', () => {
        it('should render the NavBar', () => {
            expect(wrapper).not.toBe(null);
        });

        it('should contain a div container', () => {
            expect(wrapper.find('div.navBar')).toHaveLength(1);
        });

        it('container should have 2 divs', () => {
            const children = wrapper.find('div.navBar').children();
            expect(children).toHaveLength(2);
        });

        it('check if img has a src', () => {
            const img = wrapper.find('div.navLogo');
            expect(img.find('img').prop('src')).not.toBeNull();
        });

        it('should render the correct logo', () => {
            const img = wrapper.find('div.navLogo');
            expect(img.find('img').prop('src')).toEqual(logo);
        });
    });
    describe('Testing of the Buttons',() => {
   it('should render a list with buttons',()=> {
        const list = wrapper.find("div.navLinks");
            expect(list.find('li')).toHaveLength(3);
            expect(list.find(Button)).toHaveLength(2);
            expect(list.find('Link')).toHaveLength(2);

       //    expect(list.find(Button).at(0).text()).toEqual('Over ons');
    });

    it('button activiteiten',() =>{
        const list = wrapper.find("div.navLinks");
        expect(list.find(Button).at(1).text()).toEqual('Activiteiten');
        expect(list.find('Link').at(1).props().to).toEqual('/activities');
    });

    it('button Ons',() =>{
        const list = wrapper.find("div.navLinks");
        expect(list.find(Button).at(0).text()).toEqual('Over ons');
        expect(list.find('Link').at(0).props().to).toEqual('/');
    })

  })
});

