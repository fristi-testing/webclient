import {shallow, configure} from "enzyme";
import toJson from "enzyme-to-json";
import React from "react";
import ErrorPage from "../components/Pages/ErrorPage";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });

let wrapper;

    beforeAll( () => { wrapper = shallow(<ErrorPage />); });

    describe('ErrorPage component',() =>{

    // SNAPSHOT TEST
    it('Snapshot of ErrorPage', () => {
            expect(toJson(wrapper)).toMatchSnapshot();
    });

    //UNIT TEST
    it("renders without crashing", () => {
            expect(wrapper).not.toBe(null);
    });

    it('should render a link back to homepage', () => {
            expect(wrapper.find('a').text()).toEqual('Go to Home');
            expect(wrapper.find('a').prop("href")).toEqual('/');
    });

});