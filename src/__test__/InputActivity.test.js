import React from "react";
import InputActivity from "../components/ActivityComponents/InputActivity";
import {shallow, mount, configure} from 'enzyme'
import toJson from "enzyme-to-json";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });
//global.fetch = fetch;

// Start of InputActivityComponent
describe('InputActivity Component', () => {

    let wrapper;

    beforeAll(() => { wrapper = mount(<InputActivity/>); })

    // SNAPSHOT TEST
    it('renders correctly enzyme', () => {
        const wrapper = shallow(<InputActivity />)
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    //Unit Test
    it('should render the InputActivity component',() =>{
        expect(wrapper).not.toBe(null);
    });

    describe('Form tests:', () => {
        it('input test',() =>
        {
            const inputButton = wrapper.find('input').at(0);
            console.log("inputButton =\t" + inputButton.instance().type);
            expect(inputButton.instance().type).toEqual('button');
            expect(inputButton.instance().value).toEqual('Nieuwe Activiteit');

        });

        it('Test click event', () => {

            const mockCallBack = jest.fn();
            const button = shallow((<input className="btn btn-primary" type="button" value="+" onClick={mockCallBack}/>));
            button.find('input').simulate('click');
            expect(mockCallBack.mock.calls.length).toEqual(1);
        });


       //Todo: Doesn't capture the right button
       it.skip('test toggleCollapsed', () =>{

            let inputWrapper = mount(<InputActivity collapsed={false}/>);
            expect(inputWrapper.find('input').at(0).instance().value).toEqual("-");
        })
    });

})