import React from "react";
import '@testing-library/jest-dom/extend-expect';
import GuestList from "../components/ReservationComponents/GuestList";
import mockData from "../__mocks__/mockData";
import { render, screen } from '@testing-library/react';
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });


describe('guest list test',()=>
{
    it("should show title of guests", () =>
    {
       render(<GuestList guests={mockData}/>);
       mockData.forEach((guest) => expect(screen.getByText(guest)).toBeInTheDocument());
    });

    it('should show names of guests',() =>{
        render(<GuestList guests={mockData}/>);
       // screen.debug();
    })
});











  


































