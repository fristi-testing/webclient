import {mount, shallow, configure} from "enzyme";
import React from "react";
import {render, screen} from "@testing-library/react";
import ReservationForm from "../components/ReservationComponents/ReservationForm";
import {Button} from '@material-ui/core';
import GuestListContainer from "../components/ReservationComponents/GuestListContainer";
import {beforeAll} from "@jest/globals";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });

let reservationForm;
let wrapper;

beforeAll(()=>{
    reservationForm = mount(<ReservationForm />)
    wrapper = shallow(<ReservationForm/>)

});


describe('ReservationForm tests',() => {

    it("Renders Form component", () => {
        //expect(wrapper).not.null;
        shallow(<ReservationForm/>);
    });

  /*  it("Renders Input Field", () => {
        //const { getByTestId } = render(<ReservationForm/>);
        render(<ReservationForm/>)
       // const nameInput= getByTestId('inputAmount');
        const nameInput = screen.getByRole('textbox')
        expect(nameInput).exist;
        expect(nameInput.value).toEqual('');
    });*/

    it("Renders CheckBox", () => {
        render(<ReservationForm/>)
        const checkBox= screen.getByRole('checkbox');
   //     expect(checkBox).toBeInTheDocument();
        expect(checkBox.value).toEqual("true");
    });

    it("Renders Form", () => {
        const form = reservationForm.find('Form');


       // const { getByTestId } = render(<ReservationForm/>);
      //  const form = getByTestId("formControlCheckBox");
        expect(form.find('Input')).toHaveLength(2);
    });

    it("should contain A Guest List", () => {
        const list = shallow(<GuestListContainer/>);
        expect(list).not.toBeNull();
        expect(list.length).toBe(1);

    });

    it("should contain A Save Button", () => {
        const button = reservationForm.find('Button');


       // const { getByTestId } = render(<ReservationForm/>);
       // render(<ReservationForm/>);
        //const saveBtn = screen.getByRole("button", {name: /save reservatie/i});
     //  const saveBtn = getByTestId('saveBtn');
        expect(button).toHaveLength(1);
        expect(button.text()).toEqual('Save Reservatie');


    });

// EXTRA TESTS

  /*  it('user can fill in values in the input field',()=>{
        const { getByTestId, getByText } = render(<ReservationForm/>);
        const inputAmount= getByTestId('inputAmount');
        const saveBtn = getByTestId('saveBtn');

        expect(inputAmount).toHaveValue('');
        fireEvent.change(inputAmount, {target: { value:'2'}});
        expect(inputAmount).toHaveValue('2');
        fireEvent.click(saveBtn);
        fireEvent.change(inputAmount, {target: { value:''}});
        expect(inputAmount).toHaveValue('');
    });*/

    it('should trigger the onclick',()=> {
        const mockCallback = jest.fn();
        const saveBtn = shallow( <Button data-testid="saveBtn" onClick={mockCallback}>Save Reservatie</Button>);

        saveBtn.simulate('click');
        expect(mockCallback.mock.calls.length).toEqual(1);
    })
});
