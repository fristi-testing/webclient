import React from 'react';
import { shallow,configure} from 'enzyme'
import ErrorPage from "../components/Pages/ErrorPage";
import toJson from "enzyme-to-json";
import Adapter from "enzyme-adapter-react-16";


configure({ adapter: new Adapter() });


let wrapper;

    beforeAll( () => { wrapper = shallow(<ErrorPage />); });

    describe('testing the path of the Router', () => {

    // SNAPSHOT TEST
    it('Snapshot of ErrorPage', () => { expect(toJson(wrapper)).toMatchSnapshot();});

    //UNIT TEST
    it("renders without crashing", () => {
        expect(wrapper).not.toBe(null);
    });

  //INTEGRATION TEST
   /* it('invalid path should redirect to homepage', () => {
        //Mounts the component that is linked at /random address
        const wrapper = mount(
            <MemoryRouter initialEntries={['/random']}>
                <App/>
            </MemoryRouter>
        );
        expect(wrapper.find(HomePage)).toHaveLength(0);
        expect(wrapper.find(ErrorPage)).toHaveLength(1);
    });

    it('path should go to homepage', () => {
        //Mounts the component that is linked at / address
        const wrapper = mount(
            <MemoryRouter initialEntries={['/']}>
                <App/>
            </MemoryRouter>
        );

        expect(wrapper.find(HomePage)).toHaveLength(1);
        expect(wrapper.find('div.homeContainer')).toHaveLength(1);
    });

   it('path should go to activities page', () => {
        //Mounts the component that is linked at /activities address

       const activityWrapper = mount(
                <MemoryRouter initialEntries={['/activities']}>
                    <App/>
                </MemoryRouter>
            );
        expect(activityWrapper.find(ActivityPage)).toHaveLength(1);
       // expect(activityWrapper.find(HomePage)).toHaveLength(0);

        });

   it('path should go to an activity', () => {
        //Mounts the component that is linked at /activities/{id} address
        const wrapper = mount(
            <MemoryRouter initialEntries={['/activities/1']}>
                <App/>
            </MemoryRouter>
        );

        expect(wrapper.find(ActivityDetail)).toHaveLength(1);
    })*/
});
