import {shallow} from "enzyme";
import toJson from "enzyme-to-json";
import React from "react";
import ActivityList from "../components/ActivityComponents/ActivityList";
import Adapter from 'enzyme-adapter-react-16';
import {configure} from 'enzyme'
configure({ adapter: new Adapter() });

// SNAPSHOT TEST

it('renders correctly enzyme', () => {
    const wrapper = shallow(<ActivityList />)
    expect(toJson(wrapper)).toMatchSnapshot();
});


