import React from "react";
import {shallow, mount, configure} from 'enzyme'
import InputActivityItem from "../components/ActivityComponents/InputActivityItem";
import ActivityList from "../components/ActivityComponents/ActivityList";
import toJson from "enzyme-to-json";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });

const wrapper = mount(<InputActivityItem name={'testValue'} type={'testText'}/>)

    describe('InputActivityItem',() => {

        // SNAPSHOT TEST
        it('renders correctly enzyme', () => {
            const wrapper = shallow(<ActivityList />)
            expect(toJson(wrapper)).toMatchSnapshot();
        });

        // UNIT TEST
        let formLabel= wrapper.find("FormLabel");
        let formControl = wrapper.find("FormControl");

        it('should have a formlabel',() =>{
            expect(formLabel).toHaveLength(1);
        });

        it('should give the prop of label',() =>{
         //   console.log(formLabel.text());
            expect(formLabel.text()).toEqual('testValue');
        });

        it('should give the prop of label',() =>{
         //   console.log(formLabel.text());
            expect(formLabel.text()).toEqual('testValue');
        });

        it('should give the form control', ()=>{
            expect(formControl).toHaveLength(1);
            expect(formControl.prop('type')).toEqual('testText');
            expect(formControl.prop('id')).toEqual('testValue');
        })
    });