import React from 'react';
import App from '../App';
import {shallow, configure} from 'enzyme';
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });
let wrapper;

beforeAll( () => { wrapper = shallow(<App />); })

describe('App Component', () => {

    // SNAPSHOT TEST
   // it('Snapshot', () => { expect(toJson(wrapper)).toMatchSnapshot(); });

    //UNIT TEST
    it("renders without crashing", () => {
        shallow(<App/>);
    });
/*
    // INTEGRATION TEST
    it("renders App and checks if NavBar is loaded", () => {
        const navBar = wrapper.find('div.appContainer');

        expect(navBar.find('NavBar')).toBeInTheDocument();
    });*/
});