import React from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Route, Switch

} from "react-router-dom";
import "../node_modules/fontsource-raleway/index.css"
import "../node_modules/fontsource-rosario/index.css"
//import "fontsource-rosario";
//import "fontsource-raleway";
import ActivityPage from './components/Pages/ActivityPage';
import NavBar from './components/Nav/NavBar';
import HomePage from './components/Pages/HomePage';
import ActivityDetail from './components/Pages/ActivityDetail';
import DashboardPage from './components/Dashboard/DashboardPage';
import HomeRedirect from './components/HomeRedirect';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import ProfilePage from "./components/Pages/ProfilePage";
import ErrorPage from "./components/Pages/ErrorPage";
import {Redirect} from "react-router";


document.documentElement.style.setProperty("--primaryColor", "#D43444");
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#D43444",
    },
    secondary: {
      main: "#101010",
    },
  },
});

function App() {

  return (
    <ThemeProvider theme={theme}>
      <div className="appContainer">
        <Router>
            <NavBar />
            <Switch>
                <Route exact path="/loginRedirect" component={HomeRedirect}/>
                <Route exact path="/" component={HomePage}/>
                <Route exact path="/activities" component={ActivityPage}/>
                <Route exact path="/activities/:id" component={ActivityDetail}/>
                <Route path="/dashboard/:page" component={DashboardPage}/>
                <Route exact path = "/profile" component={ProfilePage} />
                <Route exact path="/404" component={ErrorPage} />
                <Redirect from="*" to="/404"/>
            </Switch>

        </Router>
      </div>
    </ThemeProvider>
  );
}

export default App;