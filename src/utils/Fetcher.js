import { useEffect, useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";

export const Fetcher = (url, ref, dataChanged) => {

    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState(false);
    const [data, setData] = useState({});
    // Controller die requests kan stoppen (wanneer de vragende component unmount voor de request gedaan is)
    const controller = new AbortController();

    const { getAccessTokenSilently, isAuthenticated } = useAuth0();

    const fetchData = async () => {
        let token = "";
        let headers = {};
        if(isAuthenticated){ 
            token = await getAccessTokenSilently();
            headers = { 
                headers: { 
                    Authorization: `Bearer ${token}`
                    },
                signal: controller.signal
                };
        }
        console.log(process.env.REACT_APP_API_URL)
        return fetch(process.env.REACT_APP_API_URL + url, headers)
                .then(handleFetchResponse)
                .catch((e) => {
                    setError(e.name + " // " + e.message);
                });
    }

    const handleFetchResponse = (res) => {
        if(!res.ok) {
            throw new Error(res.status + ": " + res.statusText);
        }
        return res.ok && res.json ? res.json() : {};
    }

    useEffect(() => {
        if(!url) return;
        if(ref.current) {
            fetchData()
                .then((data) => {
                    setData(data);
                    setIsLoading(false);
                })
        }

        return () => ref.current = false;

    }, [url, ref]);

    useEffect(() => {
        if(dataChanged) {
            fetchData()
                .then((data) => {
                    setData(data);
                    setIsLoading(false);
                })
        }

    }, [dataChanged]);

    return { isLoading, error, data, controller };
};