import { useEffect, useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";

export const Fetcher = (url, inputMethod, inputBody) => {

    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState('');
    const [data, setData] = useState({});

    const { getAccessTokenSilently, isAuthenticated } = useAuth0();

    const fetchData = async () => {
        const token = await getAccessTokenSilently();
        return fetch(process.env.REACT_APP_API_URL + url, { headers: {
                method: inputMethod,
                body: inputMethod === "POST" || inputMethod === "PUT" ?  JSON.stringify(inputBody) : null,
                Authorization: `Bearer ${token}`}})
            .then(handleFetchResponse)
            .catch(handleFetchResponse);
    }

    const handleFetchResponse = (res) => {
        if(!res.ok) setError(res);
        return res.ok && res.json ? res.json() : {};
    }

    useEffect(() => {
        if(!url) return;

        fetchData()
            .then((data) => {
                setData(data);
                setIsLoading(false);
            });

    }, [url]);

    return { isLoading, error, data };
};