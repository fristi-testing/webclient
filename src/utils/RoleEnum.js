// "Enum" voor de verschillende rollen voor duidelijkheid en beheerbaarheid.

export const Roles = {
    "OC": "Oudercomité",
    "FIN": "Finance",
    "SPONSOR": "Sponsor",
    "ADMIN": "Admin"
}

/*
    The Object.freeze() method freezes an object. A frozen object can no longer be changed; 
    freezing an object prevents new properties from being added to it, 
    existing properties from being removed, prevents changing the enumerability, configurability,
    or writability of existing properties,
    and prevents the values of existing properties from being changed. 
    In addition, freezing an object also prevents its prototype from being changed. 
    freeze() returns the same object that was passed in.
*/
Object.freeze(Roles);