# updated SONJA
# Design and implement a container (docker image)

FROM node:14

# Create app directory
WORKDIR /usr/src/app

ENV PORT 8080
ENV HOST 0.0.0.0

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json./

#RUN npm install
RUN npm install --only=production

# If you are building your code for production (is faster)
#RUN npm ci --only=production

# Copy local code to the container
COPY . .

# map port by docker daemon
#EXPOSE 8080

# run your app and start server
#CMD ["node", "server.js"]

# Build production app
RUN npm run build

# Start the service
CMD npm start

